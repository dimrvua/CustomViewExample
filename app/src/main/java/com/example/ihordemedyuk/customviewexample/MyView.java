package com.example.ihordemedyuk.customviewexample;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;


public class MyView extends View {

    public static final int MIN_WIDTH_DP = 30;
    public static final int MIN_HEIGHT_DP = 100;
    private int maxValue = 20;
    private int minValue = 1;
    private int currentValue = 20;
    private float digitHeightPx;
    private int viewHeight;
    private int viewWidth;
    private float yStep;
    private Paint paint;
    private int touchDownY;
    private int tochDownValue;


    public MyView(Context context) {
        super(context);
        init();
    }

    public MyView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.BLUE);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        int digitsCout = maxValue - minValue + 1;
        int spacesCount = digitsCout - 1;
        digitHeightPx = h / (float) (digitsCout + spacesCount);

        viewHeight = getHeight();
        viewWidth = getWidth();

        yStep = digitHeightPx * 2;
    }

    // TODO see for more info http://stackoverflow.com/questions/12266899/onmeasure-custom-view-explanation
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = calculateSize(widthMeasureSpec, MIN_WIDTH_DP);
        int height = calculateSize(heightMeasureSpec, MIN_HEIGHT_DP);

        setMeasuredDimension(width, height);
    }

    private int calculateSize(int sizeMeasureSpec, int minSizeDp) {
        int size;
        int desiredSize = dpToPx(minSizeDp);
        int sizeMode = MeasureSpec.getMode(sizeMeasureSpec);
        int specSize = MeasureSpec.getSize(sizeMeasureSpec);
        if (sizeMode == MeasureSpec.EXACTLY) {         // fixed size or match_parent
            size = specSize;
        } else if (sizeMode == MeasureSpec.AT_MOST) {  // match parent
            size = Math.min(desiredSize, specSize);
        } else {                                        // wrap content
            size = desiredSize;
        }
        return size;
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float currentY = viewHeight - digitHeightPx;
        for (int i = minValue; i <= currentValue; i++) {
            paint.setColor(calculateColor(((float) i - minValue) / (maxValue - minValue))); // todo optimize calculations
            canvas.drawRect(0, currentY, viewWidth, currentY + digitHeightPx, paint);
            currentY -= yStep;
        }
    }

    private int calculateColor(float fraction) {
        int fromColor = Color.GREEN;
        int toColor = Color.RED;

        int fromRed = Color.red(fromColor);
        int fromGreen = Color.green(fromColor);
        int fromBlue = Color.blue(fromColor);

        int toRed = Color.red(toColor);
        int toGreen = Color.green(toColor);
        int toBlue = Color.blue(toColor);

        int resultRed = getFractionValue(fromRed, toRed, fraction);
        int resultGreen = getFractionValue(fromGreen, toGreen, fraction);
        int resultBlue = getFractionValue(fromBlue, toBlue, fraction);

        return Color.rgb(resultRed, resultGreen, resultBlue);
    }

    private int getFractionValue(int from, int to, float fraction) {
        return (int) (from + (to - from) * fraction);
    }


    // TODO handle clicks
    // TODO handle multiple touch pointers
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int eventAction = event.getAction();
        int y = (int)event.getY();

        switch (eventAction) {
            case MotionEvent.ACTION_DOWN:
                touchDownY = y;
                tochDownValue = currentValue;
                break;
            case MotionEvent.ACTION_MOVE:
                int deltaY = touchDownY - y;
                currentValue = (int) (tochDownValue + (deltaY / yStep));
                break;
        }

        invalidate();
        return true;
    }
}
